package com.example.zeger.traningandroidapp;

import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by zeger on 24/03/18.
 */

public class CustomIntentService extends IntentService{

    public CustomIntentService(){
        super("CUSTOM_INTENT_SERVICE");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Uri uri = Uri.parse(intent.getStringExtra("url"));
        try {
            URL requestURL = new URL(uri.toString());
            HttpURLConnection conn = (HttpURLConnection) requestURL.openConnection();

            ByteArrayOutputStream f = new ByteArrayOutputStream();
            int totalByte = conn.getContentLength();
            int readlen = 0;
            Long totalRead = 0l;
            byte[] buf = new byte[1024];
            while ((readlen = conn.getInputStream().read(buf)) > 0){
                f.write(buf, 0, readlen);
                totalRead += readlen;

            }
            conn.getInputStream().close();
            f.close();

            Log.d("CUSTOM_INTENT_SERVICE","COMPLETE");

            Intent bIntent = new Intent("com.example.zeger.traningandroidapp.CUSTOM_INTENT_SERVICE");

            Bundle bundle = new Bundle();
            bundle.putByteArray("bitmap",f.toByteArray());
            bIntent.putExtras(bundle);

            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(bIntent);

            //Bitmap bitmap = BitmapFactory.decodeByteArray(f.toByteArray(),0,f.size());

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
