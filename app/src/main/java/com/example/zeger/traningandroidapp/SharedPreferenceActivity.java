package com.example.zeger.traningandroidapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.zeger.traningandroidapp.entity.Kota;
import com.example.zeger.traningandroidapp.helper.KotaOpenHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by zeger on 27/03/18.
 */

public class SharedPreferenceActivity extends AppCompatActivity{
    private EditText editText3;
    private Button button2;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        button2 = findViewById(R.id.button2);
        editText3 = findViewById(R.id.editText3);

        Kota kota = new Kota();
        kota.setUrl("http://blablabla");
        kota.setNama("Jakarta");
        kota.setGambar(2);

        KotaOpenHelper kotaOpenHelper = new KotaOpenHelper(this);
        if(!kotaOpenHelper.isExist(kota)) {
            kotaOpenHelper.tambahKota(kota);
        }

        kota.setGambar(3);
        kotaOpenHelper.updateKota(kota);

        final SharedPreferences preferences = getSharedPreferences("com.example.zeger.traningandroidapp",MODE_PRIVATE);

        String username = preferences.getString("username","");

        if(!"".equals(username)){
            Intent intent = new Intent(this, MainActivity.class);

            startActivity(intent);
            finish();
        }

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("username",editText3.getText().toString());
                editor.apply();
                Intent intent = new Intent(view.getContext(), MainActivity.class);

                startActivity(intent);
                finish();
            }
        });

        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseDatabase.getReference("kota").keepSynced(true);

        firebaseDatabase.getReference("kota").child(kota.getNama())
                .child("nama").setValue(kota.getNama());
        firebaseDatabase.getReference("kota").child(kota.getNama())
                .child("gambar").setValue(kota.getGambar());
        firebaseDatabase.getReference("kota").child(kota.getNama())
                .child("url").setValue(kota.getUrl());

        /*
        FirebaseAuth.getInstance().signInWithEmailAndPassword("fitra.adinugraha@gmail.com","dreamS3ger")
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                FirebaseUser mUser = task.getResult().getUser();
                mUser.getIdToken(true)
                        .addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                            public void onComplete(@NonNull Task<GetTokenResult> task) {
                                if (task.isSuccessful()) {
                                    String idToken = task.getResult().getToken();
                                    Log.d("TOKEN",idToken);
                                    // Send token to your backend via HTTPS
                                    // ...
                                } else {
                                    // Handle error -> task.getException();
                                }
                            }
                        });
            }
        });
        */
        //auth.signInWithEmailAndPassword("fitra.adinugraha@gmail.com","dreamS3ger");

        firebaseDatabase.getReference("kota").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d("onChildAdded",dataSnapshot.getKey());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.d("onChildChanged",dataSnapshot.getKey());
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d("onChildRemoved",dataSnapshot.getKey());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.d("onChildMoved",dataSnapshot.getKey());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("onCancelled",databaseError.getMessage());
            }
        });
    }
}
