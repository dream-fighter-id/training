package com.example.zeger.traningandroidapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by zeger on 20/03/18.
 */

public class MyAsysncTaskActivity extends AppCompatActivity
{

    private ImageView imageView4;
    private ProgressBar progressBar2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myasynctask);

        imageView4 = findViewById(R.id.imageView4);
        progressBar2 = findViewById(R.id.progressBar2);
        //new MyAsyncTask().execute("https://db9uq61ujlkdm.cloudfront.net/app/uploads/sites/4/2016/09/044-Seongsan-Ilchulbong-and-Hallasan-1920x1227.jpg?x43442","https://dynaimage.cdn.cnn.com/cnn/q_auto,w_1100,c_fill,g_auto,h_619,ar_16:9/http%3A%2F%2Fcdn.cnn.com%2Fcnnnext%2Fdam%2Fassets%2F170523140714-jeju.jpg");

        DownloadReceiver receiver = new DownloadReceiver();
        IntentFilter intentFilter = new IntentFilter("com.example.zeger.traningandroidapp.DOWNLOAD");
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver,intentFilter);

        Intent downloadService = new Intent(this,DownloadIntentService.class);
        downloadService.putExtra("url","https://db9uq61ujlkdm.cloudfront.net/app/uploads/sites/4/2016/09/044-Seongsan-Ilchulbong-and-Hallasan-1920x1227.jpg?x43442");

        startService(downloadService);
    }

    class DownloadReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            byte[] byteImage = intent.getExtras().getByteArray("image");
            Bitmap bitmap = BitmapFactory.decodeByteArray(byteImage,0,byteImage.length);
            imageView4.setImageBitmap(bitmap);
        }
    }

    class MyAsyncTask extends AsyncTask<String,Integer,Bitmap>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(String... requestUrl) {

            try {
                URL url = new URL(requestUrl[0]);
                URLConnection conn = url.openConnection();

                int totalByte = conn.getContentLength();

                ByteArrayOutputStream f = new ByteArrayOutputStream();

                int readlen = 0;
                Long totalRead = 0l;
                byte[] buf = new byte[1024];
                while ((readlen = conn.getInputStream().read(buf)) > 0){
                    f.write(buf, 0, readlen);
                    totalRead += readlen;
                    publishProgress((int)(1.0 * totalRead.intValue())/totalByte * 100);
                }
                conn.getInputStream().close();
                f.close();

                Bitmap bitmap = BitmapFactory.decodeByteArray(f.toByteArray(),0,f.size());
                return bitmap;
            }catch (IOException e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            progressBar2.setMax(100);
            progressBar2.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            imageView4.setImageBitmap(bitmap);
        }
    }
}

/*
ByteArrayOutputStream bytes = new ByteArrayOutputStream();
							bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

							//you can create a new file name "test.jpg" in sdcard folder.
							FileOutputStream f = new FileOutputStream(filename);
							InputStream isBitmap = new ByteArrayInputStream(bytes.toByteArray());

							int readlen = 0;
							Long totalRead = 0l;
							byte[] buf = new byte[1024];
							while ((readlen = isBitmap.read(buf)) > 0){
								f.write(buf, 0, readlen);
								totalRead += readlen;
								publishProgress(totalRead.intValue());
							}
							f.close();
							isBitmap.close();
							bytes.close();
 */