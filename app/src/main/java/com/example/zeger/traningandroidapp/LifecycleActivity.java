package com.example.zeger.traningandroidapp;

import android.content.Intent;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LifecycleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lifecycle);
        Log.d("LIFECYCLE","onCreate");

        Button button = findViewById(R.id.button4);
        final EditText editText = findViewById(R.id.editText5);

        Log.d("savedInstanceState",savedInstanceState+"");
        if(savedInstanceState!=null){
            int visibility = savedInstanceState.getInt("showStatus");
            if(visibility==View.GONE){
                editText.setVisibility(View.GONE);
            }
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intent = new Intent(LifecycleActivity.this, Main2Activity.class);
                //startActivity(intent);
                if(editText.getVisibility()==View.GONE){
                    editText.setVisibility(View.VISIBLE);
                }else {
                    editText.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("LIFECYCLE","onRestart");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        final EditText editText = findViewById(R.id.editText5);
        outState.putInt("showStatus",editText.getVisibility());

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("LIFECYCLE","onResume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("LIFECYCLE","onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("LIFECYCLE","onDestroy");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("LIFECYCLE","onStart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("LIFECYCLE","onPause");
    }
}
