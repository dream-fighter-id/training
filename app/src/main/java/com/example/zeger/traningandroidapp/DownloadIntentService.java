package com.example.zeger.traningandroidapp;

import android.app.IntentService;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by zeger on 24/03/18.
 */

public class DownloadIntentService extends IntentService{
    public DownloadIntentService(){
        super("DownloadIntentService");
    }
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try {
            URL url = new URL(intent.getStringExtra("url"));
            URLConnection conn = url.openConnection();

            int totalByte = conn.getContentLength();

            ByteArrayOutputStream f = new ByteArrayOutputStream();

            int readlen = 0;
            Long totalRead = 0l;
            byte[] buf = new byte[1024];
            while ((readlen = conn.getInputStream().read(buf)) > 0){
                f.write(buf, 0, readlen);
                totalRead += readlen;
                //publishProgress((int)(1.0 * totalRead.intValue())/totalByte * 100);
            }
            conn.getInputStream().close();
            f.close();

            //Bitmap bitmap = BitmapFactory.decodeByteArray(f.toByteArray(),0,f.size());

            Bundle bundle = new Bundle();
            bundle.putByteArray("image",f.toByteArray());

            Intent broadcast = new Intent("com.example.zeger.traningandroidapp.DOWNLOAD");
            broadcast.putExtras(bundle);

            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(broadcast);

            Log.d("DownloadIntentService","COMPLETED");
            //return bitmap;
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
