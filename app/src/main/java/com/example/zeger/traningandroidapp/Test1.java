package com.example.zeger.traningandroidapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by zeger on 03/03/18.
 */

public class Test1 extends AppCompatActivity{
    EditText name;
    EditText jumlah;
    Button kereta;
    Button pesawat;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gorengan);
        name = findViewById(R.id.nama);
        jumlah = findViewById(R.id.jumlah);
        kereta = findViewById(R.id.kereta);
        pesawat = findViewById(R.id.pesawat);

        kereta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Test1.this, Main2Activity.class);
                intent.putExtra("nama",name.getText());
                intent.putExtra("jumlah",jumlah.getText());
                int harga = Integer.parseInt(jumlah.getText().toString()) * 300000;
                intent.putExtra("harga",harga);

                startActivity(intent);
            }
        });

        pesawat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Test1.this, Main2Activity.class);
                intent.putExtra("nama",name.getText());
                intent.putExtra("jumlah",jumlah.getText());
                int harga = Integer.parseInt(jumlah.getText().toString()) * 500000;
                intent.putExtra("harga",harga);

                startActivity(intent);
            }
        });
    }
}
