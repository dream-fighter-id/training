package com.example.zeger.traningandroidapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by zeger on 03/03/18.
 */

public class Test2 extends AppCompatActivity{
    TextView nama;
    TextView jumlah;
    TextView harga;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gorengan1);

        nama = findViewById(R.id.nama);
        jumlah = findViewById(R.id.jumlah);
        harga = findViewById(R.id.harga);

        if(getIntent()!=null){
            nama.setText(getIntent().getStringExtra("nama"));
            jumlah.setText(getIntent().getStringExtra("jumlah"));
            harga.setText(""+getIntent().getIntExtra("harga",0));

            Toast.makeText(Test2.this,"",Toast.LENGTH_LONG).show();
        }
    }
}
