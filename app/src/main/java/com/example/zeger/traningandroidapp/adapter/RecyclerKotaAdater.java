package com.example.zeger.traningandroidapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zeger.traningandroidapp.R;
import com.example.zeger.traningandroidapp.entity.Kota;

/**
 * Created by zeger on 11/03/18.
 */

public class RecyclerKotaAdater extends RecyclerView.Adapter<RecyclerKotaAdater.ViewHolder>{
    private Kota[] arrKota;

    public RecyclerKotaAdater(Kota[] arrKota){
        this.arrKota = arrKota;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item_kota,
                        parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder,
                                 int position) {
        Kota kota = arrKota[position];
        holder.gambarIV.setImageResource(kota
                .getGambar());
        holder.namaIV.setText(kota.getNama());
    }

    @Override
    public int getItemCount() {
        return arrKota.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView gambarIV;
        TextView namaIV;

        public ViewHolder(View itemView) {
            super(itemView);
            ImageView gambarIV = itemView
                    .findViewById(R.id.gambar_iv);
            TextView namaIV = itemView
                    .findViewById(R.id.nama_tv);


            this.gambarIV = gambarIV;
            this.namaIV = namaIV;
        }
    }
}
