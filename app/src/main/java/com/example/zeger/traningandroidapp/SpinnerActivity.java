package com.example.zeger.traningandroidapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.zeger.traningandroidapp.adapter.KotaAdapter;
import com.example.zeger.traningandroidapp.adapter.RecyclerKotaAdater;
import com.example.zeger.traningandroidapp.entity.Kota;

public class SpinnerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);

        Spinner spinner = findViewById(R.id.spinner);
        ListView listView = findViewById(R.id.listView);
        RecyclerView recyclerView = findViewById(R.id.recyclerview);

        Kota[] arrKota = new Kota[1000];
        for(int i=0;i<1000;i++){
            Kota kota1 = new Kota();
            kota1.setNama("Bandung");
            kota1.setGambar(R.drawable.ic_bandung);
            arrKota[i] = kota1;
        }

        /*
        Kota kota1 = new Kota();
        kota1.setNama("Bandung");
        kota1.setGambar(R.drawable.ic_bandung);

        Kota kota2 = new Kota();
        kota2.setNama("Cimahi");
        kota2.setGambar(R.drawable.ic_cimahi);

        Kota kota3 = new Kota();
        kota3.setNama("Buah Batu");
        kota3.setGambar(R.drawable.ic_buahbatu);

        arrKota[0] = kota1;
        arrKota[1] = kota2;
        arrKota[2] = kota3;
        */
        KotaAdapter kotaAdapter = new KotaAdapter(arrKota);
        RecyclerKotaAdater recyclerKotaAdater = new RecyclerKotaAdater(arrKota);

        //ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
        //        android.R.layout.simple_spinner_item);
        //arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //arrayAdapter.addAll(arrString);

        spinner.setAdapter(kotaAdapter);
        listView.setAdapter(kotaAdapter);

        //recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL));
        recyclerView.setAdapter(recyclerKotaAdater);
    }
}
