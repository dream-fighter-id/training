package com.example.zeger.traningandroidapp.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.zeger.traningandroidapp.entity.Kota;

/**
 * Created by zeger on 01/04/18.
 */

public class TableKotaOpenHelper extends SQLiteOpenHelper{
    private static int VERSION = 1;
    private static String DATABASE_NAME = "table_kota";

    private static String TABLE_NAME = "table_kota";

    private static String COLUMN_ID = "_id";
    private static String COLUMN_NAME = "name";
    private static String COLUMN_GAMBAR = "gambar";
    private static String COLUMN_URL = "url";

    public TableKotaOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String q = "create table "+TABLE_NAME+"(" +
                "    "+COLUMN_ID+" Integer primary key," +
                "    "+COLUMN_NAME+" text," +
                "    "+COLUMN_GAMBAR+" Integer," +
                "    "+COLUMN_URL+" text" +
                "    ) ";

        sqLiteDatabase.execSQL(q);
    }

    public long insert(Kota kota){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME,kota.getNama());
        contentValues.put(COLUMN_GAMBAR,kota.getGambar());
        contentValues.put(COLUMN_URL,kota.getUrl());

        Log.d("DB","here");
        return db.insert(TABLE_NAME,null,contentValues);
    }

    public long update(Kota kota){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME,kota.getNama());
        contentValues.put(COLUMN_GAMBAR,kota.getGambar());
        contentValues.put(COLUMN_URL,kota.getUrl());

        Log.d("DB","here");
        return db.update(TABLE_NAME,contentValues,COLUMN_NAME + "=?",new String[]{kota.getNama()});
    }

    public boolean isExist(Kota kota){
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME + " where " + COLUMN_NAME + "= ?", new String[]{kota.getNama()});
        return cursor.getCount() > 0;
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
