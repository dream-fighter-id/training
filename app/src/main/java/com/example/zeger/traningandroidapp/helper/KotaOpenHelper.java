package com.example.zeger.traningandroidapp.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.zeger.traningandroidapp.entity.Kota;

/**
 * Created by zeger on 31/03/18.
 */

public class KotaOpenHelper extends SQLiteOpenHelper{
    private static int DB_VERSION = 1;
    private static String DB_NAME = "training";
    private static String TABLE_NAME = "kota";

    private static String COLUMN_ID = "_id";
    private static String COLUMN_NAMA = "nama";
    private static String COLUMN_URL = "url";
    private static String COLUMN_GAMBAR = "gambar";

    public KotaOpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String q = "create table "+TABLE_NAME+"(" +
                "    "+COLUMN_ID+" integer primary key not null," +
                "    "+COLUMN_NAMA+" text," +
                "    "+COLUMN_GAMBAR+" integer," +
                "    "+COLUMN_URL+" text" +
                ")";

        sqLiteDatabase.execSQL(q);
    }

    public long tambahKota(Kota kota){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAMA,kota.getNama());
        contentValues.put(COLUMN_GAMBAR,kota.getGambar());
        contentValues.put(COLUMN_URL,kota.getUrl());

        return db.insert(TABLE_NAME,null,contentValues);
    }

    public long updateKota(Kota kota){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAMA,kota.getNama());
        contentValues.put(COLUMN_GAMBAR,kota.getGambar());
        contentValues.put(COLUMN_URL,kota.getUrl());

        return db.update(TABLE_NAME,contentValues,COLUMN_NAMA + "=?",new String[]{kota.getNama()});
    }

    public boolean isExist(Kota kota){
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("select * from "+TABLE_NAME+" where "+COLUMN_NAMA+"=?",new String[]{kota.getNama()});

        return cursor.getCount() > 0;
    }

    public Kota[] findAllKota(){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from "+TABLE_NAME,null);
        Kota[] kotas = new Kota[cursor.getCount()];

        while (cursor.moveToNext()){
            String nama = cursor.getString(1);
            int gambar = cursor.getInt(2);
            String url = cursor.getString(3);

            Kota kota = new Kota();
            kota.setNama(nama);
            kota.setGambar(gambar);
            kota.setUrl(url);
            kotas[cursor.getPosition()] = kota;
        }

        return kotas;
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
