package com.example.zeger.traningandroidapp.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.View;

import com.example.zeger.traningandroidapp.R;
import com.example.zeger.traningandroidapp.entity.Kota;
import com.example.zeger.traningandroidapp.fragment.ScreenPageFragment;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;

/**
 * Created by zeger on 25/03/18.
 */

public class ScreenPagerAdapter extends FragmentPagerAdapter {

    private Kota[] arrKota = new Kota[3];

    public ScreenPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        try {
            Resources res = context.getResources();
            InputStream in_s = res.openRawResource(R.raw.kota);

            byte[] b = new byte[in_s.available()];
            in_s.read(b);
            String kotaJson = new String(b);
            Log.d("kotaJson",kotaJson);

            JSONObject jsonKota = new JSONObject(kotaJson);
            JSONArray data = jsonKota.getJSONArray("data");
            for(int i=0;i<data.length();i++){
                JSONObject jKota = data.getJSONObject(i);
                String nama = jKota.getString("nama");
                String url = jKota.getString("url");

                Log.d("kotaJson","nama="+nama);
                Log.d("kotaJson","url="+url);

                Kota kota = new Kota();
                kota.setNama(nama);
                kota.setUrl(url);
                arrKota[i] = kota;
            }

        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    @Override
    public Fragment getItem(int position) {
        return ScreenPageFragment.newInstance(arrKota[position]);
    }

    @Override
    public int getCount() {
        return arrKota.length;
    }
}
