package com.example.zeger.traningandroidapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.zeger.traningandroidapp.adapter.ScreenPagerAdapter;

/**
 * Created by zeger on 25/03/18.
 */

public class ScreenPagerActivity extends AppCompatActivity{

    private ViewPager viewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_pager);

        viewPager = findViewById(R.id.pager);
        viewPager.setOffscreenPageLimit(5);

        ScreenPagerAdapter adapter = new ScreenPagerAdapter(this,getSupportFragmentManager());
        viewPager.setAdapter(adapter);

    }
}
