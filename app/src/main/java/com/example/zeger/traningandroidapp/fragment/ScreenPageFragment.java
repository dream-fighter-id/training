package com.example.zeger.traningandroidapp.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.zeger.traningandroidapp.ImageDownloadTaskActivity;
import com.example.zeger.traningandroidapp.R;
import com.example.zeger.traningandroidapp.entity.Kota;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by zeger on 25/03/18.
 */

public class ScreenPageFragment extends Fragment{

    private ProgressBar progressBar4;
    private ImageView imageView6;

    public static ScreenPageFragment newInstance(Kota kota) {

        Bundle args = new Bundle();
        args.putString("nama_kota",kota.getNama());
        args.putString("url",kota.getUrl());
        ScreenPageFragment fragment = new ScreenPageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_screen_page, container,false);
        TextView textView2 = view.findViewById(R.id.textView2);
        progressBar4 = view.findViewById(R.id.progressBar4);
        imageView6 = view.findViewById(R.id.imageView6);


        textView2.setText(getArguments().getString("nama_kota"));
        ImageDownloadTask task = new ImageDownloadTask();
        task.execute(getArguments().getString("url"));
        return view;
    }

    class ImageDownloadTask extends AsyncTask<String,Integer,Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            String url = strings[0];
            Uri builtURI = Uri.parse(url);
            try {
                URL requestURL = new URL(builtURI.toString());
                HttpURLConnection conn =
                        (HttpURLConnection) requestURL.openConnection();
                conn.connect();

                InputStream is = conn.getInputStream();

                ByteArrayOutputStream f = new ByteArrayOutputStream();
                int totalByte = conn.getContentLength();// total download

                int readlen = 0;//buat nampung download per byte
                Long totalRead = 0l; // total yang sudah di download

                byte[] buf = new byte[1024]; // buffer buat ngewrite ke output stream

                while ((readlen = is.read(buf)) > 0){
                    f.write(buf, 0, readlen);
                    totalRead += readlen;
                    publishProgress((int)(1.0 * totalRead.intValue()) / totalByte * 100);
                }
                is.close();
                f.close();

                Bitmap bitmap = BitmapFactory.decodeByteArray(f.toByteArray(),0,totalByte);

                return bitmap;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            imageView6.setImageBitmap(bitmap);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            progressBar4.setProgress(values[0]);
        }
    }
}
