package com.example.zeger.traningandroidapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by zeger on 25/03/18.
 */

public class ImageDownloadTaskActivity extends AppCompatActivity{
    private ProgressBar progressBar3;
    private ImageView imageView5;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_task);

        progressBar3 = findViewById(R.id.progressBar3);
        imageView5 = findViewById(R.id.imageView5);

        // check sdcard ada atau gak
        if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            File image = new File(path,"gambar.jpg");
            if(image.exists()){
                Bitmap b = BitmapFactory.decodeFile(image.getAbsolutePath());
                imageView5.setImageBitmap(b);
            }else{
                ImageDownloadTask task = new ImageDownloadTask();
                task.execute("http://foto2.data.kemdikbud.go.id/getImage/20247027/7.jpg","","","","");
            }
        }
    }

    class ImageDownloadTask extends AsyncTask<String,Integer,Bitmap>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            String url = strings[0];
            Uri builtURI = Uri.parse(url);
            try {
                URL requestURL = new URL(builtURI.toString());
                HttpURLConnection conn =
                        (HttpURLConnection) requestURL.openConnection();
                conn.connect();

                InputStream is = conn.getInputStream();

                ByteArrayOutputStream f = new ByteArrayOutputStream();
                int totalByte = conn.getContentLength();// total download

                int readlen = 0;//buat nampung download per byte
                Long totalRead = 0l; // total yang sudah di download

                byte[] buf = new byte[1024]; // buffer buat ngewrite ke output stream

                while ((readlen = is.read(buf)) > 0){
                    f.write(buf, 0, readlen);
                    totalRead += readlen;
                    publishProgress((int)(1.0 * totalRead.intValue()) / totalByte * 100);
                }
                is.close();
                f.close();

                if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                    File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                    File image = new File(path,"gambar.jpg");
                    FileOutputStream fo = new FileOutputStream(image);
                    fo.write(f.toByteArray());
                    fo.close();
                }

                Bitmap bitmap = BitmapFactory.decodeByteArray(f.toByteArray(),0,totalByte);

                return bitmap;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            imageView5.setImageBitmap(bitmap);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            progressBar3.setProgress(values[0]);
        }
    }
}
