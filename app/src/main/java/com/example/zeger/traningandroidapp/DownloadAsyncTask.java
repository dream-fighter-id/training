package com.example.zeger.traningandroidapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by zeger on 22/03/18.
 */

public class DownloadAsyncTask extends AppCompatActivity {

    private ProgressBar progressBar2;
    private ImageView imageView4;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_myasynctask);

        progressBar2 = findViewById(R.id.progressBar2);
        imageView4 = findViewById(R.id.imageView4);

        //new DownloadTask().execute("https://d3h30waly5w5yx.cloudfront.net/images/tour/pictures/tubester5.jpg","http://dreamfighter.web.id/android/data.json","https://d3h30waly5w5yx.cloudfront.net/images/tour/pictures/tubester5.jpg","https://d3h30waly5w5yx.cloudfront.net/images/tour/pictures/tubester5.jpg");

        CustomReceiver reciever = new CustomReceiver();
        IntentFilter intentFilter = new IntentFilter("com.example.zeger.traningandroidapp.CUSTOM_INTENT_SERVICE");

        LocalBroadcastManager.getInstance(this).registerReceiver(reciever,intentFilter);


        Intent customService = new Intent(this, CustomIntentService.class);
        customService.putExtra("url","https://d3h30waly5w5yx.cloudfront.net/images/tour/pictures/tubester5.jpg");

        startService(customService);

    }

    class CustomReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            byte[] byteBitmap = intent.getExtras().getByteArray("bitmap");
            Bitmap bitmap = BitmapFactory.decodeByteArray(byteBitmap,0,byteBitmap.length);
            imageView4.setImageBitmap(bitmap);
        }
    }

    class DownloadTask extends AsyncTask<String,Integer,String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            Uri uri = Uri.parse(strings[1]);
            try {
                URL requestURL = new URL(uri.toString());
                HttpURLConnection conn = (HttpURLConnection) requestURL.openConnection();

                ByteArrayOutputStream f = new ByteArrayOutputStream();
                int totalByte = conn.getContentLength();
                int readlen = 0;
                Long totalRead = 0l;
                byte[] buf = new byte[1024];
                while ((readlen = conn.getInputStream().read(buf)) > 0){
                    f.write(buf, 0, readlen);
                    totalRead += readlen;
                    publishProgress((int)(1.0 * totalRead.intValue())/totalByte * 100);
                }
                conn.getInputStream().close();
                f.close();

                //Bitmap bitmap = BitmapFactory.decodeByteArray(f.toByteArray(),0,f.size());
                return new String(f.toByteArray());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            progressBar2.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(String json) {
            //Log.d("JSON",json);


            try {
                JSONObject jsonObject = new JSONObject(json);

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i=0;i<jsonArray.length();i++){
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    String phone = jsonObject1.getString("phone");

                    Log.d("JSON","phone="+phone);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            //imageView4.setImageBitmap(bitmap);
        }
    }
}
