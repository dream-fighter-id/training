package com.example.zeger.traningandroidapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {
    private Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        TextView textView = findViewById(R.id.textView);

        Button buttonClose = findViewById(R.id.button3);

        ImageView imageView = findViewById(R.id.imageView3);

        getIntent().getStringExtra("name");


        if(getIntent()!=null) {
            Intent intent = getIntent();
            String username = intent.getStringExtra("username");
            int nim = intent.getIntExtra("nim", 0);

            textView.setText(username);
        }


        if(savedInstanceState!=null){
            extras = savedInstanceState;
            Bitmap bitmap = (Bitmap)savedInstanceState.get("data");
            if(bitmap!=null){
                imageView.setImageBitmap(bitmap);
            }
        }


        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Uri uri = Uri.parse("http://detik.com");
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                startActivityForResult(intent,1);
            }
        });

        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent data = new Intent();
                data.putExtra("success","login berhasil");
                setResult(0,data);

                finish();
            }
        });
    }

    /*
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        ImageView imageView = findViewById(R.id.imageView3);
        if(savedInstanceState!=null){
            extras = savedInstanceState;
            Bitmap bitmap = (Bitmap)savedInstanceState.get("data");
            if(bitmap!=null){
                imageView.setImageBitmap(bitmap);
            }
        }
    }
    */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bitmap = (Bitmap) data.getExtras().get("data");

        extras = data.getExtras();

        ImageView imageView = findViewById(R.id.imageView3);
        imageView.setImageBitmap(bitmap);

        Log.d("IMAGE","" + bitmap);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(extras!=null){
            outState.putAll(extras);
        }
        super.onSaveInstanceState(outState);
    }
}
