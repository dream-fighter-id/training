package com.example.zeger.traningandroidapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.zeger.traningandroidapp.entity.Kota;
import com.example.zeger.traningandroidapp.helper.TableKotaOpenHelper;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Kota kota = new Kota();
        kota.setNama("Cicaheum");
        kota.setGambar(2);
        kota.setUrl("http://foto2.data.kemdikbud.go.id/getImage/20247027/7.jpg");

        TableKotaOpenHelper tableKotaOpenHelper = new TableKotaOpenHelper(this);

        if(!tableKotaOpenHelper.isExist(kota)) {
            tableKotaOpenHelper.insert(kota);
        }else{
            tableKotaOpenHelper.update(kota);
            Log.d("KOTA","Sudah ada!");
        }

        final EditText usernameET = findViewById(R.id.editText3);
        Button button = findViewById(R.id.button2);

        SharedPreferences preferences = getSharedPreferences("com.example.zeger.traningandroidapp",
                MODE_PRIVATE);
        String username = preferences.getString("username","");

        // untuk memonitor perubahan di shared preferences
        preferences.registerOnSharedPreferenceChangeListener(new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {

            }
        });

        if(!username.equals("")){
            Intent intent = new Intent(LoginActivity.this, Main2Activity.class);
            int nim = 1828282;
            intent.putExtra("username",username);
            intent.putExtra("nim", nim);
            startActivityForResult(intent,0);
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, Main2Activity.class);

                String username = usernameET.getText().toString();
                int nim = 1828282;
                intent.putExtra("username",username);
                intent.putExtra("nim", nim);

                SharedPreferences preferences = getSharedPreferences("com.example.zeger.traningandroidapp", MODE_PRIVATE);

                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("username",username);
                editor.putInt("nim",nim);
                //editor.apply();
                editor.commit();


                startActivityForResult(intent,0);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("RESULT",resultCode + "");
        if(data!=null) {
            String success = data.getStringExtra("success");
            Log.d("RESULT", "success=" + success);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}
