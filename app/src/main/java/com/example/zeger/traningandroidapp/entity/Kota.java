package com.example.zeger.traningandroidapp.entity;

/**
 * Created by zeger on 11/03/18.
 */

public class Kota {
    private String nama;
    private int gambar;
    private String url;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getGambar() {
        return gambar;
    }

    public void setGambar(int gambar) {
        this.gambar = gambar;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
