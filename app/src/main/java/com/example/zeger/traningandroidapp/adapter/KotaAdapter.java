package com.example.zeger.traningandroidapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zeger.traningandroidapp.R;
import com.example.zeger.traningandroidapp.entity.Kota;

/**
 * Created by zeger on 11/03/18.
 */

public class KotaAdapter extends BaseAdapter{
    private Kota[] arrKota;

    public KotaAdapter(Kota[] arrKota){
        this.arrKota = arrKota;
    }

    @Override
    public int getCount() {
        return arrKota.length;
    }

    @Override
    public Kota getItem(int i) {
        return arrKota[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view,
                        ViewGroup viewGroup) {
        ViewHolder vh;
        if(view==null) {
            view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.layout_item_kota,
                            viewGroup, false);
            ImageView gambarIV = view
                    .findViewById(R.id.gambar_iv);
            TextView namaIV = view.findViewById(R.id.nama_tv);


            vh = new ViewHolder();
            vh.gambarIV = gambarIV;
            vh.namaIV = namaIV;
            view.setTag(vh);
        }else{
            vh = (ViewHolder)view.getTag();
        }

        Kota kota = getItem(i);
        vh.gambarIV.setImageResource(kota.getGambar());
        vh.namaIV.setText(kota.getNama());
        return view;
    }

    class ViewHolder{
        ImageView gambarIV;
        TextView namaIV;
    }
}
